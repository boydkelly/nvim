local map = vim.keymap.set
local opts = { noremap = true, silent = true }
vim.g.mutton_status = 0
vim.g.translate_source = "fr"
vim.g.translate_target = "en"
vim.g.translate_winsize = 10

--
--vimwiki
--vim.g.vimwiki_list = { { path='~/wiki/', syntax='markdown', ext='.md' } }
--map('n', '<leader>ww', ':e /home/bkelly/wiki/index.md<cr>', opts )
--
--wiki.vim
vim.g.wiki_root = "/home/bkelly/wiki.vim"
vim.g.wiki_cache_persistent = 1
vim.g.wiki_filetypes = { "adoc" }
vim.g.wiki_link_target_type = "adoc_xref_inline"
vim.g.wiki_write_on_nav = 1
map("n", "<leader>vv", ":e /home/bkelly/wiki.vim/index.adoc<cr>", opts)
vim.g.nv_search_paths = { "~/Documents/Jula" }
--
--vviki
vim.g.vviki_root = "/var/home/bkelly/wiki"
vim.g.vviki_ext = ".adoc"
vim.g.vviki_links_include_ext = 1
map("n", "<leader>vv", ":e /home/bkelly/wiki/index.adoc<cr>", opts)
map("n", "<leader>vv", ":e /home/bkelly/Documents/Jula/index.adoc<cr>", opts)

vim.env.FZF_DEFAULT_OPTS = "--layout=reverse"
vim.env.FZF_DEFAULT_OPTS = "--ansi --preview-window 'right:60%' --layout reverse --margin=1,4"
vim.env.FZF_DEFAULT_COMMAND = 'rg --files --ignore-case --hidden -g "!{.git,node_modules,vendor}/*"'
vim.env.FZF_DEFAULT_OPTS =
  "--ansi --preview-window 'right:60%' --layout reverse --margin=1,4 --preview 'bat --color=always --style=header,grid --line-range :300 {}'"
-- need to convert this to lua create user command
-- command! -bang -nargs=? -complete=dir Files
--      \ call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)

-- vim-templates plugin
-- plugin disabled for now
-- vim.g.tmpl_search_paths = '~/.config/nvim/templates'
