return {
  {
    "nvim-lua/plenary.nvim",
    "nvim-lua/popup.nvim",
    "MunifTanjim/nui.nvim",
    { "tpope/vim-repeat", event = "InsertEnter" },
    { "nacro90/numb.nvim", event = "BufReadPre", config = true },

    {
      "andymass/vim-matchup",
      event = { "BufReadPost" },
      init = function()
        vim.g.matchup_matchparen_offscreen = { method = "popup" }
      end,
    },
    {
      "dstein64/vim-startuptime",
      enabled = true,
      cmd = "StartupTime",
      init = function()
        vim.g.startuptime_tries = 10
      end,
    },
    {
      "folke/persistence.nvim",
      event = "BufReadPre",
      opts = { options = { "buffers", "curdir", "tabpages", "winsize", "help" } },
    },
    -- for french quotes
    -- { "reedes/vim-textobj-quote", ft = { "asciidoc", "markdown" }, event = "BufReadPost" },
    -- { "kana/vim-textobj-user", ft = { "asciidoc", "markdown" }, event = "BufReadPost" },
    -- { "reedes/vim-lexical", ft = { "asciidoc", "markdown", "text" }, event = "BufReadPost" },
    -- { "dpelle/vim-LanguageTool", ft = { "asciidoc", "markdown" }, event = "BufReadPost" },
    { "boydkelly/vim-asciidoctor", branch = "asciidoc", ft = "asciidoc", },
    { "mustache/vim-mustache-handlebars", ft = "html" },
    { "neo4j-contrib/cypher-vim-syntax", ft = { "cypher" } },
    { "chrisbra/unicode.vim", cmd = "UnicodeSearch" },
    { "ledger/vim-ledger", ft = { "ledger" } },
    { "mechatroner/rainbow_csv", enabled = true, ft = { "csv", "tsv" } },
    { "chrisbra/csv.vim", enabled = false, ft = { "csv", "tsv" } },
  },
}
