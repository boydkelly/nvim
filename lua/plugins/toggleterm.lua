return {
  "akinsho/toggleterm.nvim",
  cmd = { "ToggleTerm", "TermExec" },
  config = function()
    require ("setup.toggleterm")
  end
}
