return {
  {
    "stevearc/dressing.nvim",
    enabled = true,
    lazy = false,
    config = ("setup.dressing")
  },
}
