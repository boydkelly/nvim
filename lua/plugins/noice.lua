return {
  {
    "folke/noice.nvim",
    event = "VimEnter",
    config = function()
      require("setup.noice")
    end
  },
}
