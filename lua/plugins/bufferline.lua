return {
  {
    "akinsho/nvim-bufferline.lua",
    event = "VeryLazy",
    config = function()
      require("setup.bufferline")
    end
  },
  -- local options = opts.options
  -- if vim.opt.mousemoveevent then
  -- 	options.hover = {
  -- 		enabled = true,
  -- 		delay = 100,
  -- 		reveal = { "close" },
  -- 	}
  -- end
  -- scope buffers to tabs
  { "tiagovla/scope.nvim", event = "VeryLazy", opts = {} },
}
