return {
  {
    "NeogitOrg/neogit",
    cmd = "Neogit",
    branch = "nightly",
    config = function()
      require("setup.neogit")
    end
  },
  {
    "sindrets/diffview.nvim",
    event = "VeryLazy",
    cmd = { "DiffviewOpen", "DiffviewClose", "DiffviewToggleFiles", "DiffviewFocusFiles" },
    config = true,
  },
  {
    "kevinhwang91/nvim-ufo",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "kevinhwang91/promise-async",
      {
        "luukvbaal/statuscol.nvim",
        config = function()
          require ("setup.statuscol")
        end
      },
    },
    event = "BufReadPost",
    config = ("setup.ufo")
  },
  {
    "lewis6991/gitsigns.nvim",
    event = "BufReadPre",
    config=function()
      require ("setup.gitsigns")
    end
  },
  {
    "ThePrimeagen/git-worktree.nvim",
    opts = {},
    config = function()
      require("telescope").load_extension "git_worktree"
    end,
    dependencies = {
      "nvim-telescope/telescope.nvim",
    },
    --stylua: ignore
    keys = {
      {"<leader>gwm", function() require("telescope").extensions.git_worktree.git_worktrees() end, desc = "Manage"},
      {"<leader>gwc", function() require("telescope").extensions.git_worktree.create_git_worktree() end, desc = "Create"},
    },
  },
  {
    "akinsho/git-conflict.nvim",
    --stylua: ignore
    keys = {
      -- {"<leader>gT", function() require("plugins.hydra.git-action").open_git_conflict_hydra() end, desc = "Conflict"},
      -- {"<leader>gS", function() require("plugins.hydra.git-action").open_git_signs_hydra() end, desc = "Signs"},
    },
    cmd = {
      "GitConflictChooseBoth",
      "GitConflictNextConflict",
      "GitConflictChooseOurs",
      "GitConflictPrevConflict",
      "GitConflictChooseTheirs",
      "GitConflictListQf",
      "GitConflictChooseNone",
      "GitConflictRefresh",
    },
    opts = {
      default_mappings = true,
      default_commands = true,
      disable_diagnostics = true,
    },
  },
  {
    "folke/trouble.nvim",
    cmd = { "TroubleToggle", "Trouble" },
    config=function()
      require ("setup.trouble")
    end
  },
}
