return {
  {
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    config = function()
      require("setup.c-nvim-autopairs")
    end
  },
}
