return {
  {
    "echasnovski/mini.surround",
    event = "VimEnter",
    enabled = true,
    config = function(_, _)
      require("mini.surround").setup()
    end,
  },
  {
    "echasnovski/mini.animate",
    event = "VimEnter",
    enabled = true,
    config = function(_, _)
      require("mini.animate").setup()
    end,
  },
  {
    "echasnovski/mini.bracketed",
    event = "VimEnter",
    config = function()
      require("mini.bracketed").setup()
    end,
  },
  {
    "echasnovski/mini.indentscope",
    event = { "BufReadPre", "BufNewFile" },
    opts = {
      symbol = "│",
      options = { try_as_border = true },
    },
    init = function()
      vim.api.nvim_create_autocmd("FileType", {
        pattern = { "help", "alpha", "dashboard", "NvimTree", "NeoTree", "Trouble", "lazy", "mason" },
        callback = function()
          vim.b.miniindentscope_disable = true
        end,
      })
    end,
    config = function(_, opts)
      require("mini.indentscope").setup(opts)
    end,
  },
}
