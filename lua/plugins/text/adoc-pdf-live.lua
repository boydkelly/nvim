return {
  {
    "marioortizmanero/adoc-pdf-live.nvim",
    ft = "asciidoc",
    enabled = false,
    config = function()
      require("adoc_pdf_live").setup {
        enabled = false,
        viewer = "zathura",
        binary = "asciidoctor-pdf",
        params = "",
        debug = false,
        style = "",
        style_regex = "style\\.ya?ml",
      }
    end,
  },
}
