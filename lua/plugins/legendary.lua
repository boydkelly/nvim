return {
  {
    "mrjones2014/legendary.nvim",
    cmd = "Legendary",
    keys = {
      { "<leader>hC", "<cmd>Legendary<cr>", desc = "Command Palette" },
    },
    priority = 10000,
    lazy = false,
    config = ("setup.legendary")
  },
}
