return {
  "hrsh7th/nvim-cmp",
  event = "CmdlineEnter",
  dependencies = {
    {
      "L3MON4D3/LuaSnip",
      config = function()
        require("setup.a-luasnip")
      end
    },
    {
      "saadparwaiz1/cmp_luasnip",
    },
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "petertriho/cmp-git",
    "kirasok/cmp-hledger",
    "f3fora/cmp-spell",
    { "hrsh7th/cmp-cmdline", enabled = true },
    { "dmitmel/cmp-cmdline-history", enabled = false },
    "hrsh7th/cmp-calc",
  },
  config = function()
    require("setup.b-nvim-cmp")
  end
}

