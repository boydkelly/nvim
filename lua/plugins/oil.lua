return {
  "stevearc/oil.nvim",
  event = "VeryLazy",
  config = function()
    require("setup.oil")
  end
}
