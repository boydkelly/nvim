return {
    "mfussenegger/nvim-lint",
    event = "BufReadPre",
    config =function()
      require ("setup.lint")
    end
}
