return {
  {
    "nvim-neorg/neorg",
    dependencies = { "luarocks.nvim" },
    lazy = false,
    config = function()
      require("setup.neorg")
    end
  }
}
