return {
  "j-hui/fidget.nvim",
  enabled = true,
  config = function()
    require("setup.fidget")
  end
}
