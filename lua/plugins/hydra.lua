return {
  "nvimtools/hydra.nvim",
  lazy = false,
  config = function()
    require "setup.hydra"
  end,
}
