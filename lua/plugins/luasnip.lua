return {
    "L3MON4D3/LuaSnip",
    event = "BufReadPre",
    dependencies = {
      {
        "rafamadriz/friendly-snippets",
      },
    },
    build = "make install_jsregexp",
}
