require ("config.telescope.keymaps")
return {
    "nvim-telescope/telescope.nvim",
    dependencies = {
      { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
      {"mrjones2014/legendary.nvim"},
      "nvim-telescope/telescope-file-browser.nvim",
      "ahmedkhalf/project.nvim", -- projects
      "cljoly/telescope-repo.nvim",
      "tsakirist/telescope-lazy.nvim",
      "nvim-telescope/telescope-symbols.nvim",
      "octarect/telescope-menu.nvim",
      "nvim-telescope/telescope-live-grep-args.nvim",
    },
    cmd = "Telescope",
  config = function()
    require("setup.telescope")
  end
}
