return {
  {
    "nvim-orgmode/orgmode",
    -- ft = { "org" },
    -- opts = {
    --   org_agenda_files = { "~/gtd/pages/*" },
    --   org_default_notes_file = "~/gtd/pages/Contents.org",
    --   -- org_agenda_files = { vim.env.HOME .. "/org-notes/agenda/*" },
    --   -- org_default_notes_file = vim.env.HOME .. "/org-notes/default.org",
    -- },
    config = function()
      require("setup.orgmode")
    end
    -- config = function(opts)
    --   require("orgmode").setup_ts_grammar()
    --   require("nvim-treesitter.configs").setup {
    --     -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
    --     org_todo_keywords = { "NOW", "LATER", "|", "DONE" },
    --     highlight = {
    --       enable = true,
    --       additional_vim_regex_highlighting = { "org" }, -- Required since TS highlighter doesn't support all syntax features (conceal)
    --     },
    --     ensure_installed = { "org" }, -- Or run :TSUpdate org
    --   }
    --
    --   require("orgmode").setup(opts)
    -- end,
  },
  { "akinsho/org-bullets.nvim" },
}
