return {
  {
    "nvim-treesitter/nvim-treesitter",
    tag = "v0.9.2",
    dependencies = {
      "nvim-treesitter/nvim-treesitter-textobjects",
      "JoosepAlviste/nvim-ts-context-commentstring",
      "RRethy/nvim-treesitter-endwise",
      {"windwp/nvim-ts-autotag",
        config = function()
          require("setup.ts-autotag")
        end
      },
    },
    build = ":TSUpdate",
    event = { "InsertEnter" },
    config = function()
      require("setup.treesitter")
    end
  }
}

