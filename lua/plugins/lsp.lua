return {
  -- LSP0 config copied from lzpzero gitlab.
  -- Only added custom yaml
  {
    "VonHeikemen/lsp-zero.nvim",
    lazy = true,
  },
  {
    "williamboman/mason.nvim",
    lazy = false,
    config = true,
  },
  {
    'WhoIsSethDaniel/mason-tool-installer.nvim',
    lazy = false,
    config = function()
      local conf = require "config"
      require("mason-tool-installer").setup {
        ensure_installed = conf.tools,
        auto_update = true,
        run_on_start = true,
        start_delay = 3000, -- 3 second delay
        debounce_hours = 5 -- at least 5 hours bet
      }
    end,
  },
  {
    "neovim/nvim-lspconfig",
    cmd = { "LspInfo", "LspInstall", "LspStart" },
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      { "hrsh7th/cmp-nvim-lsp" },
      { "williamboman/mason-lspconfig.nvim" },
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      { "j-hui/fidget.nvim", opts = {} },
    },
    config = function()
      require("setup.b-lsp")
    end,
  },
  {
    "Bekaboo/dropbar.nvim",
    event = "VeryLazy",
    enabled = true,
    config = true
  },
}
