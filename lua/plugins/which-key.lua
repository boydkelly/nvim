return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  config = function()
    require("setup.which-key")
  end
}
