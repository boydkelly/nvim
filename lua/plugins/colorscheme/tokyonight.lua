return {
  {
    "folke/tokyonight.nvim",
    enabled = true,
    event = "CmdlineEnter",
    config = function()
      require("setup.colorscheme.tokyonight")
    end
    -- config = function()
    --   local tokyonight = require "tokyonight"
    --   tokyonight.setup {
    --     style = "moon",
    --     sidebars = {
    --       "qf",
    --       "vista_kind",
    --       "terminal",
    --       "spectre_panel",
    --       "startuptime",
    --       "Outline",
    --     },
    --     on_highlights = function(hl, c)
    --       local prompt = "#2d3149"
    --       hl.CursorLineNr = { fg = c.orange, bold = true }
    --       hl.TelescopeNormal = { bg = c.bg_dark, fg = c.fg_dark }
    --       hl.TelescopeBorder = { bg = c.bg_dark, fg = c.bg_dark }
    --       hl.TelescopePromptNormal = { bg = prompt }
    --       hl.TelescopePromptBorder = { bg = prompt, fg = prompt }
    --       hl.TelescopePromptTitle = { bg = c.fg_gutter, fg = c.orange }
    --       hl.TelescopePreviewTitle = { bg = c.bg_dark, fg = c.bg_dark }
    --       hl.TelescopeResultsTitle = { bg = c.bg_dark, fg = c.bg_dark }
    --     end,
    --   }
    -- end,
  },
}
