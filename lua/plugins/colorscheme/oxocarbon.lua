return {
  {
    "boydkelly/oxocarbon.nvim",
    enabled = true,
    priority = 3000,
    lazy = false,
    branch = "dev2",
    config = function()
      vim.cmd.colorscheme "oxocarbon"
    end,
  },
}
