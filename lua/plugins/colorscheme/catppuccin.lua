return {
  "catppuccin/nvim",
  enabled = true,
  event = "CmdlineEnter",
  name = "catppuccin",
  config = function()
    require("setup.colorscheme.catppucin")
  end,
}
