local templates = vim.api.nvim_create_augroup("templates", { clear = true })
local config = vim.fn.stdpath('config')
vim.api.nvim_create_autocmd(
  { "BufNewFile" },
  { pattern = { "*.sh" }, command = '0r' .. config .. '/templates/skeleton.sh',  group = templates }
)
vim.api.nvim_create_autocmd(
  { "BufNewFile" },
  { pattern = { "*.md" }, command = '0r' .. config .. '/templates/skeleton.md', group = templates }
)
vim.api.nvim_create_autocmd(
  { "BufNewFile" },
  { pattern = { "*.xml" }, command = '0r' .. config .. '/templates/skeleton.xml', group = templates }
)
vim.api.nvim_create_autocmd(
  { "BufNewFile" },
  { pattern = { "*.norg" }, command = '0r' .. config ..  '/templates/skeleton.norg', group = templates }
)

local gitGroup
vim.api.nvim_create_augroup("gitGroup", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
  group = gitGroup,
  pattern = { "gitcommit" },
  command = [[
    setl spell
    ]],
})

local inputMethod
vim.api.nvim_create_autocmd({ "VimEnter", "InsertLeave", "CmdlineLeave" }, {
  group = inputMethod,
  pattern = { "*" },
  callback = function()
    os.execute "gdbus call --session --dest org.gnome.Shell --object-path /dev/ramottamado/EvalGjs --method dev.ramottamado.EvalGjs.Eval 'imports.ui.status.keyboard.getInputSourceManager().inputSources[0].activate()' &>/dev/null"
  end,
})

vim.api.nvim_create_autocmd({ "VimLeave", "InsertEnter", "CmdlineEnter" }, {
  group = inputMethod,
  pattern = { "*.adoc", "*.md", "*.txt" },
  callback = function()
    os.execute "gdbus call --session --dest org.gnome.Shell --object-path /dev/ramottamado/EvalGjs --method dev.ramottamado.EvalGjs.Eval 'imports.ui.status.keyboard.getInputSourceManager().inputSources[1].activate()'&>/dev/null"
  end,
})
--
--
-- local lexical
-- vim.api.nvim_create_augroup("lexical", { clear = true })
-- vim.api.nvim_create_autocmd("FileType", {
-- 	group = lexical,
-- 	pattern = { "text" },
-- 	command = "call lexical#init({ 'spell': 1 })",
-- })
--
-- vim.api.nvim_create_autocmd({ "BufEnter", "BufNew" }, {
-- 	group = lexical,
-- 	pattern = { "*.adoc", "*.md", "*.tex" },
-- 	--  command = [[lua require('functions')prose]]
-- 	command = "Prose",
-- 	--  callback = require('functions')prose
-- })

-- local lexicon
-- vim.api.nvim_create_autocmd({ "Bufsave" }, {
--   group = lexicon,
--   pattern = { "lexicon.yml" },
--   command = [[lua require("core.utils.functions").update_meta()]],
-- })
--
-- user commands

-- and require it like normal in your lua file
vim.api.nvim_create_user_command("Neat", function()
  local neat = require "is_neat" -- compiled & cached on demand
  neat "fennel" -- => "fennel is neat!"
end, { nargs = 0, desc = "What is neet" })

vim.api.nvim_create_user_command("Prose", function()
  require("utils").prose()
end, { nargs = 0, desc = "Apply prose settings" })
--
-- vim.api.nvim_create_user_command("ToggleBar", function()
--   require("utils.functions").toggleBar()
-- end, { nargs = 0, desc = "Toggle Bar" })
--
-- vim.api.nvim_create_user_command("ToggleGuillemets", function()
--   require("utils.functions").toggleGuillemets()
-- end, { nargs = 0, desc = "Toggle toggleGuillemets" })
--
-- vim.api.nvim_create_user_command("Translate", function()
--   require("utils.functions").translate()
-- end, { nargs = 0, desc = "Translation settings" })
--
-- vim.api.nvim_create_user_command("Mandenkan", function()
--   require("config.telescope").mandenkan()
-- end, { nargs = 0, desc = "Search Mandenkan" })

-- conform
vim.api.nvim_create_user_command("Format", function(args)
  local range = nil
  if args.count ~= -1 then
    local end_line = vim.api.nvim_buf_get_lines(0, args.line2 - 1, args.line2, true)[1]
    range = {
      start = { args.line1, 0 },
      ["end"] = { args.line2, end_line:len() },
    }
  end
  require("conform").format({ async = true, lsp_fallback = true, range = range })
end, { range = true })

vim.api.nvim_create_user_command("FormatDisable", function(args)
  if args.bang then
    -- FormatDisable! will disable formatting just for this buffer
    vim.b.disable_autoformat = true
  else
    vim.g.disable_autoformat = true
  end
end, {
  desc = "Disable autoformat-on-save",
  bang = true,
})

vim.api.nvim_create_user_command("FormatEnable", function()
  vim.b.disable_autoformat = false
  vim.g.disable_autoformat = false
end, {
  desc = "Re-enable autoformat-on-save",
})
