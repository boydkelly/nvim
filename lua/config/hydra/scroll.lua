require "hydra" {
  name = "Side scroll",
  mode = "n",
  body = "z",
  hint = "Side scroll",
  heads = {
    { "h", "5zh" },
    { "l", "5zl", { desc = "←/→" } },
    { "H", "zH" },
    { "L", "zL", { desc = "half screen ←/→" } },
  },
}
