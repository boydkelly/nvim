local Hydra = require "hydra"

local function cmd(command)
  return table.concat { "<Cmd>", command, "<CR>" }
end

local hint = [[
 _j_: next                 _k_: previous
 _a_: add word             _l_: list corrections   
 _f_: use first correction

 ^
]]

Hydra {
  name = "Spelling",
  hint = hint,
  config = {
    color = "amaranth",
    invoke_on_body = true,
    hint = {
      position = "bottom",
    },
  },
  mode = "n",
  body = "<leader>z",
  heads = {
    { "j", "]s" },
    { "k", "[s" },
    { "a", "zg" },
    { "l", "z=" },
    { "l", cmd "Telescope spell_suggest" },
    -- {
    --   "l",
    --   function()
    --     require("telescope.builtin").spell_suggest()
    --   end,
    --   { desc = "Spelling Suggestions" },
    -- },
    { "f", "1z=" },
    { "<esc>", nil, { exit = true } },
    { "q", nil, { exit = true, mode = "n" } },
  },
}
