return {
    capabilities = {
      textDocument = {
        foldingRange = {
          dynamicRegistration = false,
          lineFoldingOnly = true,
        },
      },
    },
    settings = {
      redhat = { telemetry = { enabled = false } },
      yaml = {
        keyOrdering = false,
        format = {
          enable = true,
          proseWrap = "always",
          printWidth = 80
        },
        schemas = {
          ["/home/bkelly/dev/jula/dyu-xdxf/mandenkan/xdxf_strict.json"] = "lexicon.{yml,yaml}",
        },
        validate = { enable = true },
        completion = true,
        hover = true,
        schemaStore = {
          -- Must disable built-in schemaStore support to use
          -- schemas from SchemaStore.nvim plugin
          enable = false,
          -- Avoid TypeError: Cannot read properties of undefined (reading 'length')
          url = "",
        },
      },
    },
  }
