local icons = require "config.icons"
local config = {
  diagnostic = {
    source = true,
    goto_prev = { float = false },
    update_in_insert = false,
    severity_sort = true,
    underline = {
      severity = {
        min = vim.diagnostic.severity.INFO,
      },
    },
    signs = {
      severity = {
        min = vim.diagnostic.severity.HINT,
      },
    },
    float = {
      enabled = false,
      source = true,
      show_header = true,
      header = { icons.diagnostics.Debug .. " Diagnostics:", "DiagnosticInfo" },
      focusable = false,
    },
    virtual_text = {
      source = false,
      spacing = 2,
      prefix = "●",
      severity = {
        min = vim.diagnostic.severity.HINT,
      },
    },
  },
}

return config
