vim.g.logging_level = "info"
vim.g.loaded_perl_provider = 0  -- Disable perl provider
vim.g.loaded_ruby_provider = 0  -- Disable ruby provider
vim.g.loaded_node_provider = 0  -- Disable node provider
-- Whether to load netrw by default, see https://github.com/bling/dotvim/issues/4
vim.g.loaded_netrw       = 1
vim.g.loaded_netrwPlugin = 1
vim.g.netrw_liststyle = 3

-- Do not load the following system plugins
-- Does this even work?
vim.g.loaded_2html_plugin = 1
vim.g.loaded_zipPlugin = 1
vim.g.loaded_gzip = 1
vim.g.loaded_tarPlugin = 1
vim.g.loaded_tutor_mode_plugin = 1
vim.g.loaded_matchit = 1
vim.g.loaded_matchparen = 1
vim.g.loaded_bugreport = 1
vim.g.loaded_compiler = 1
vim.g.loaded_getscript = 1
vim.g.loaded_getscriptPlugin = 1

-- local builtin_plugs = {
--    'getscript',
--    'getscriptPlugin',
--  }
--  for i = 1, #builtin_plugs do
--    g['loaded_' .. builtin_plugs[i]] = true
--  end
