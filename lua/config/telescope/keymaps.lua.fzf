local utils = require "utils"
-- keymap("n", "<S-h>", "<cmd>BufferLineCyclePrev<cr>", { { desc = "Prev buffer" })
-- vim.keymap.set("n", "<leader>;", "<cmd>Telescope menu theme=dropdown<cr>", { desc = "Menu", remap = true })
  -- code
vim.keymap.set("n", "<leader>bj", "<cmd>BufferLinePick<cr>", { desc = "Jump" })
vim.keymap.set("n", "<leader>bf", "<cmd>FzfLua buffers<cr>", { desc = "Find" })
vim.keymap.set("n", "<leader>fb", "<cmd>FzfLua buffers<cr>", { desc = "Find" })
vim.keymap.set("n", "<leader>cp", "<cmd>FzfLua diagnostics_document <cr>", { desc = "Document diagnostics" })
vim.keymap.set("n", "<leader>cP", "<cmd>FzfLua diagnostics_workspace<cr>", { desc = "Workspace diagnostics" })
  -- utils.telescope("find_files", { cwd = vi.fn.stdpath("config") })
vim.keymap.set("n", "<leader>fc", "<cmd>FzfLua files cwd=~/.config<cr>", { desc = "XDG config files" })
vim.keymap.set("n", "<leader>fC", "<cmd>FzfLua files cwd=~/.config/nvim-rocks<cr>", { desc = "Neovim config files" })

vim.keymap.set("n", "<leader>ff", require("utils").telescope "files", { desc = "Find Files (Root Dir)" })
vim.keymap.set("n", "<leader>fF", require("utils").telescope("files", { cwd = false }), { desc = "Find Files (Cwd)" })
vim.keymap.set("n", "<leader>fm", "<cmd>FzfLua marks<cr>", { desc = "Marks" })
vim.keymap.set("n", "<leader>fp", "<cmd>lua require('fzf-lua').files({ cwd = vim.fn.stdpath 'state' })<cr>", { desc = "Installed plugins" })
vim.keymap.set("n", "<leader>fr", "<cmd>FzfLua oldfiles<CR>", { desc = "Recent Files" })
vim.keymap.set("n", "<leader>fs", require("luasnip.loaders").edit_snippet_files, { desc = "Edit snippets" })

  -- help
vim.keymap.set("n", "<leader>hs", "<cmd>FzfLua help_tags<cr>", { desc = "Help tags" })
vim.keymap.set("n", "<leader>hm", "<cmd>FzfLua man_pages<cr>", { desc = "Man pages" })
-- vim.keymap.set("n", "<leader>hp", "<cmd>Telescope lazy<cr>", { desc = "Plugin readme's" })
  -- project
  -- {
  --   "<leader>pp",
  --   function()
  --     require("telescope").extensions.project.project { display_type = "minimal" }
  --   end,
  --   { desc = "Project [ctrl + d b a s l r f w o]",
  -- })
vim.keymap.set("n", "<leader>pp", "<cmd>Telescope projects theme=dropdown<cr>", { desc = "Projects [ctrl + f b d s r w]" })
  -- { "<leader>pP", require("telescope").extensions.projects.projects { { desc = "Projects [ctrl + f b d s r w]" } })
vim.keymap.set("n", "<leader>pr", "<cmd>Telescope repo list<cr>", { desc = "Dynamic projects repo" })
vim.keymap.set("n", "<leader>pc", "<cmd>cd %:p:h<cr>", { desc = "Change WorkDir to current file" })

  -- search

vim.keymap.set("n", "<leader>sb",
    function()
      require("telescope.builtin").current_buffer_fuzzy_find()
    end,
    { desc = "Buffer", })
  -- { "<leader>sb", utils.telescope("current_buffer_fuzzy_find", { cwd = true }), { desc = "Buffer" })
  -- { "<leader>sb", require("telescope.builtin").current_buffer_fuzzy_find(), "Buffer" })
  --search
  --vim.keymap.set("n",
  --   "<leader>fg",
  --   function()
  --     require("telescope").extensions.live_grep_args.live_grep_args()
  --   end,
  --   { desc = "Live Grep",
  -- })
vim.keymap.set("n", "<leader>sc", "<cmd>Telescope commands<cr>", { desc = "Commands" })
vim.keymap.set("n",
    "<leader>sf",
    "<cmd>lua require'telescope.builtin'.grep_string{ shorten_path = true, word_match = '-w', only_sort_text = true, search = '' }<cr>",
    { desc = "Fuzzy search",
  })
vim.keymap.set("n", "<leader>sh", "<cmd>Telescope heading<cr>", { desc = "Headings" })
vim.keymap.set("n", "<leader>sH", "<cmd>Telescope highlights<cr>", { desc = "Search Highlight Groups" })
vim.keymap.set("n", "<leader>sJ", "<cmd>lua require('config.telescope').mande()<cr>", { desc = "Search Jula" })
vim.keymap.set("n", "<leader>sK", "<cmd>lua require('config.telescope').manden_string()<cr>", { desc = "??Search mande keyword" })
vim.keymap.set("n", "<leader>sk", "<cmd>Telescope keymaps<cr>", { desc = "Keymaps" })
vim.keymap.set("n", "<leader>sl", "<cmd>Telescope resume<cr>", { desc = "Resume last search" })
vim.keymap.set("n", "<leader>sm", "<cmd>lua require('config.telescope').mandenkan()<cr>", { desc = "Search Mandenkan" })
vim.keymap.set("n", "<leader>sO", "<cmd>Telescope vim_options<cr>", { desc = "Vim Options" })
vim.keymap.set("n", "<leader>sR", "<cmd>Telescope registers<cr>", { desc = "Registers" })
vim.keymap.set("n", "<leader>ss", "<cmd>Telescope grep_string<cr>", { desc = "Text under cursor" })
vim.keymap.set("n", "<leader>sS", "<cmd>Telescope symbols<cr>", { desc = "Symbols" })
vim.keymap.set("n", "<leader>s:", "<cmd>Telescope search_history<cr>", { desc = "Search History" })
vim.keymap.set("n", "<leader>s;", "<cmd>Telescope command_history<cr>", { desc = "Command history" })
vim.keymap.set("n",
    "<leader>su",
    function()
      require("telescope.builtin").live_grep { search_dirs = { vim.fs.dirname(vim.fn.expand "%") } }
    end,
    { desc = "Grep (Current File Path)",
  })
  -- { "<leader>sw", require("utils").telescope "live_grep", { desc = "Grep (Root Dir)" })
  -- { "<leader>sW", require("utils").telescope("live_grep", { cwd = false }), { desc = "Grep (Cwd)" })
vim.keymap.set("n", "<leader>sw", utils.telescope("grep_string", { word_match = "-w" }), { desc = "Word (root dir)" })
vim.keymap.set("n", "<leader>sW", utils.telescope("grep_string", { cwd = false, word_match = "-w" }), { desc = "Word (cwd)" })
vim.keymap.set("n", "<leader>sw", utils.telescope("grep_string", { mode = "v"}), { desc = "Selection (root dir)" })
vim.keymap.set("n", "<leader>sW", utils.telescope("grep_string", { cwd = false, mode = "v"}), { desc = "Selection (cwd)" })
vim.keymap.set("n", "<leader>st", "<cmd>Telescope live_grep<cr>", { desc = "Strings" })
vim.keymap.set("n", "<leader>sU", "<cmd>lua require('config.telescope').unicode()<cr>", { desc = "Search Unicode" })
  -- Ui
vim.keymap.set("n", "<leader>uC", utils.telescope("colorscheme", { enable_preview = true }), { desc = "Colorscheme with preview" })
vim.keymap.set("n", "<leader>uc;", "<cmd>Telescope commands<cr>", { desc = "Run Command" })
