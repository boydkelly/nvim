-- "folke/noice.nvim",
local icons = require "config.icons"
local focused = true
vim.api.nvim_create_autocmd("FocusGained", {
  callback = function()
    focused = true
  end,
})
vim.api.nvim_create_autocmd("FocusLost", {
  callback = function()
    focused = false
  end,
})
require("noice").setup {
  debug = false,
  presets = {
    bottom_search = false,
    cmdline_output_to_split = false,
    command_palette = true,
    inc_rename = true,
    long_message_to_split = false,
    lsp_doc_border = false,
  },
  cmdline = {
    enabled = true,
    view = "cmdline",
    format = {
      cmdline = { icon = icons.ui.Cmdline },
      search_down = { icon = " " .. icons.ui.Search_down },
      search_up = { icon = " " .. icons.ui.Search_up },
      filter = { icon = icons.ui.Filter },
      lua = { icon = icons.ui.Lua },
      help = { icon = icons.ui.Help },
    },
  },
  messages = {
    -- NOTE: If you enable messages, then the cmdline is enabled automatically.
    -- This is a current Neovim limitation.
    enabled = true, -- enables the Noice messages UI
    view = "mini", -- default view for messages
    view_error = "mini", -- view for errors
    view_warn = "mini", -- view for warnings
    view_history = "popup", -- view for :messages
  },
  popupmenu = {
    enabled = true, -- enables the Noice popupmenu UI
    backend = "nui", -- backend to use to show regular cmdline completions
    border = {
      padding = { 0, 1 }
    },
    position = "auto",
    relative = "editor",
    size = {
      height = "auto",
      max_height = 20,
      width = "auto"
    },
    win_options = {
      cursorline = true,
      cursorlineopt = "line",
      foldenable = false,
      winbar = "",
      winhighlight = {
        CursorLine = "NoicePopupmenuSelected",
        FloatBorder = "NoicePopupmenuBorder",
        Normal = "NoicePopupmenu",
        PmenuMatch = "NoicePopupmenuMatch"
      }
    },
    zindex = 65
  },
  redirect = {
    view = "popup",
    filter = { event = "msg_show" },
  },
  commands = {
    last = {
      view = "split",
      -- options for the message history that you get with `:Noice`
      opts = { enter = true, format = "details" },
      filter = {},
    },
    all = {
      -- options for the message history that you get with `:Noice`
      view = "popup",
      opts = { enter = true, format = "details" },
      filter = {},
    },
  },
  notify = {
    -- Noice can be used as `vim.notify` so you can route any notification like other messages
    -- Notification messages have their level and other properties set.
    -- event is always "notify" and kind can be any log level as a string
    -- The default routes will forward notifications to nvim-notify
    -- Benefit of using Noice for this is the routing and consistent history view
    enabled = true,
    view = "mini",
  },
  lsp = {
    progress = { enabled = false },
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
    },
    -- hover and signature conflict with file installed by neovim nightly
    hover = {
      enabled = false,
      view = nil, -- when nil, use defaults from documentation
      opts = {}, -- merged with defaults from documentation
    },
    signature = {
      enabled = false,
      auto_open = {
        enabled = true,
        trigger = true, -- Automatically show signature help when typing a trigger character from the LSP
        luasnip = true, -- Will open signature help when jumping to Luasnip insert nodes
        throttle = 50, -- Debounce lsp signature help request by 50ms
      },
    },
  },
  views = {
    confirm = {
      win_options = {
        winhighlight = "NormalFloat:NormalFloat,FloatBorder:FloatBorder",
      },
      position = {
        row = "100%",
        col = 0,
      },
      size = {
        width = "100%",
        height = "auto",
      },
    },
    cmdline_popup = {
      border = {
        style = "none",
        padding = { 1, 0 },
      },
      filter_options = {},
      win_options = {
        winhighlight = "NormalFloat:NormalFloat,FloatBorder:FloatBorder",
      },
      position = {
        row = 0,
        col = 1,
      },
      size = {
        width = "100%",
      },
    },
  },
  routes = {
    -- {
    --   filter = { event = "msg_showmode" },
    --   view = "mini",
    -- },
    {
      filter = {
        event = "msg_show",
        find = "change",
      },
      opts = { skip = true },
    },
    {
      filter = {
        event = "msg_show",
        find = "Col",
      },
      opts = { skip = true },
    },
    {
      filter = {
        event = "msg_show",
        find = "lines",
      },
      opts = { skip = true },
    },
    {
      filter = {
        event = "notify.warn",
        find = "Change",
      },
      opts = { skip = true },
    },
  },
  format = {},
}
vim.keymap.set("c", "<S-Enter>", function()
  require("noice").redirect(vim.fn.getcmdline())
end, { desc = "Redirect Cmdline" })

vim.keymap.set("n", "<leader>hnl", function()
  require("noice").cmd "last"
end, { desc = "Noice Last Message" })

vim.keymap.set("n", "<leader>hnh", function()
  require("noice").cmd "history"
end, { desc = "Noice History" })

vim.keymap.set("n", "<leader>hna", function()
  require("noice").cmd "all"
end, { desc = "Noice All" })

vim.keymap.set("n", "<c-f>", function()
  if not require("noice.lsp").scroll(4) then
    return "<c-f>"
  end
end, { silent = true, expr = true })

vim.keymap.set("n", "<c-b>", function()
  if not require("noice.lsp").scroll(-4) then
    return "<c-b>"
  end
end, { silent = true, expr = true })

vim.api.nvim_create_autocmd("FileType", {
  pattern = "markdown",
  callback = function(event)
    vim.schedule(function()
      require("noice.text.markdown").keys(event.buf)
    end)
  end,
})
