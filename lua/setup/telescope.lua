-- "nvim-telescope/telescope.nvim",

-- dependencies = {
--   { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
--   {
--     "mrjones2014/legendary.nvim",
--   },
--   "nvim-telescope/telescope-file-browser.nvim",
--   "ahmedkhalf/project.nvim", -- projects
--   "cljoly/telescope-repo.nvim",
--   "tsakirist/telescope-lazy.nvim",
--   "nvim-telescope/telescope-symbols.nvim",
--   "octarect/telescope-menu.nvim",
--   "nvim-telescope/telescope-live-grep-args.nvim",
-- },
-- stylua: ignore

require("config.telescope.keymaps")
local t = require "telescope"
local icons = require "config.icons"
local actions = require "telescope.actions"
local themes = require "telescope.themes"
local action_layout = require "telescope.actions.layout"
local conf = require "config.telescope"

local opts = {
  defaults = {
    prompt_prefix = icons.ui.Search,
    selection_caret = icons.arrows.ArrowRight .. " ",
    mappings = {
      i = {
        -- Close on first esc instead of going to normal mode
        -- https://github.com/nvim-telescope/telescope.nvim/blob/master/lua/telescope/mappings.lua
        ["<esc>"] = actions.close,
        ["<C-j>"] = actions.move_selection_next,
        ["<PageUp>"] = actions.results_scrolling_up,
        ["<PageDown>"] = actions.results_scrolling_down,
        ["<C-u>"] = actions.preview_scrolling_up,
        ["<C-d>"] = actions.preview_scrolling_down,
        ["<C-k>"] = actions.move_selection_previous,
        ["<C-q>"] = actions.send_selected_to_qflist,
        ["<C-l>"] = actions.send_to_qflist,
        ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
        ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
        ["<cr>"] = actions.select_default,
        ["<c-v>"] = actions.select_vertical,
        ["<c-s>"] = actions.select_horizontal,
        ["<c-t>"] = actions.select_tab,
        ["<c-p>"] = action_layout.toggle_preview,
        ["<c-o>"] = action_layout.toggle_mirror,
        ["<c-h>"] = actions.which_key,
        ["<c-x>"] = actions.delete_buffer,
      },
    },
    themes.get_dropdown {
      layout_config = {
        horizontal = {
          height = 0.75,
          width = 1,
          preview_width = 0.57,
          prompt_position = "bottom",
        },
      },
      path_display = "smart",
    },

    -- used for grep_string and live_grep
    vimgrep_arguments = {
      "rg",
      "-L",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
      "--no-ignore",
      "--trim",
    },
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    multi_icon = "● ",
    scroll_strategy = "cycle",
    sorting_strategy = "ascending",
    layout_strategy = "flex",
    dynamic_preview_title = true,
    layout_config = {
      horizontal = {
        prompt_position = "bottom",
        preview_width = 0.55,
      },
      vertical = {
        mirror = false,
      },
      width = 0.87,
      height = 0.8,
      preview_cutoff = 10,
    },
    use_less = true,
    -- file_ignore_patterns = { "node_modules", "%.jpg", "%.png" },
    file_ignore_patterns = conf.file_ignore_patterns,
    winblend = 0,
    border = {},
    borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
    color_devicons = true,
    path_display = { "smart" },
  },
  pickers = {
    spell_suggest = { theme = "cursor" },
    find_files = {
      hidden = true,
      find_command = { "rg", "--files", "--hidden", "-g", "!.git" },
    },
    git_files = {
      previewer = true,
    },
    keymaps = {
      theme = "dropdown",
    },
    buffers = {
      theme = "dropdown",
      previewer = false,
      ignore_current_buffer = true,
      sort_lastused = true,
    },
    live_grep = {
      prompt_title = "Find in file",
      title = "Preview",
    },
    oldfiles = {
      prompt_title = "Recent files",
    },
    -- find_command = { "fd", "--hidden", "--type", "file", "--follow", "--strip-cwd-prefix" },
    find_command = { "rg", "--files", "--hidden", "--glob", "!.git/*" },
  },
  extensions = require "config.telescope.extensions",
}
t.setup(opts)
-- t.load_extension "fzf"
t.load_extension "file_browser"
t.load_extension "projects"
-- t.load_extension "menu"
-- t.load_extension "repo"
t.load_extension "live_grep_args"
