local opts = {
  plugins = {
    presets = { z = true, g = true },
    spelling = { enabled = true },
  },
  key_labels = {
    ["<space>"] = "SPC",
    ["<cr>"] = "RET",
    ["<CR>"] = "RET",
    ["<Tab>"] = "TAB",
  },
  window = {
    margin = { 0, 0, 0, 0 },
    padding = { 0, 0, 0, 0 },
  },
  hidden = {
    "<silent>",
    "<cmd>",
    "<Cmd>",
    "<cr>",
    "<CR>",
    "call",
    "lua",
    "require",
    "Plug",
    "^:",
    "^ ",
  },
  groups = {
    mode = { "n", "v" },
    ["<leader>b"] = { name = "Buffer" },
    ["<leader>c"] = { name = "Code" },
    ["<leader>f"] = { name = "File" },
    ["<leader>g"] = { name = "Git" },
    ["<leader>h"] = { name = "Help" },
    ["<leader>n"] = { name = "Notes" },
    ["<leader>p"] = { name = "Project" },
    ["<leader>s"] = { name = "Search" },
    ["<leader>S"] = { name = "Session" },
    ["<leader>u"] = { name = "Ui" },
    ["<leader>um"] = { name = "Manage plugins" },
    ["<leader>uk"] = { name = "Keyboard" },
    ["<leader>un"] = { name = "Neovim options" },
    ["<leader>v"] = { name = "View" },
  },
}
local wk = require("which-key")
wk.setup(opts)
wk.register(opts.groups)
