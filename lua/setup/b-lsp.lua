-- LSP0 config copied from lzpzero gitlab.
-- Only added custom yaml/xml
-- "neovim/nvim-lspconfig",
-- This is where all the LSP shenanigans will live
vim.g.lsp_zero_extend_cmp = 0
-- vim.g.lsp_zero_extend_lspconfig = 0
local lsp_zero = require "lsp-zero"
lsp_zero.extend_lspconfig()

--- if you want to know more about lsp-zero and mason.nvim
--- read this: https://github.com/VonHeikemen/lsp-zero.nvim/blob/v3.x/doc/md/guides/integrate-with-mason-nvim.md
lsp_zero.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp_zero.default_keymaps { buffer = bufnr }
end)

-- mason
local conf = require "config"
require("mason").setup() -- needed to add this for rocks config
require("mason-tool-installer").setup {
  ensure_installed = conf.tools,
  auto_update = true,
  run_on_start = true,
  start_delay = 3000, -- 3 second delay
  debounce_hours = 5 -- at least 5 hours bet
}
require("mason-lspconfig").setup {
  ensure_installed = conf.lsp_servers,
  handlers = {
    lsp_zero.default_setup,
    lua_ls = function()
      -- (Optional) Configure lua language server for neovim
      local lua_opts = lsp_zero.nvim_lua_ls()
      require("lspconfig").lua_ls.setup(lua_opts)
    end,
    lemminx = function()
      local xml_opts = require "config.lsp.xml3"
      require("lspconfig").lemminx.setup(xml_opts)
    end,
    yamlls = function()
      local yaml_opts = require "config.lsp.yaml3"
      require("lspconfig").yamlls.setup(yaml_opts)
    end,
  },
}
