-- "mrjones2014/legendary.nvim",
-- cmd = "Legendary",
-- keys = {
--   { "<leader>hC", "<cmd>Legendary<cr>", desc = "Command Palette" },
-- },
-- priority = 10000,
-- lazy = false,
-- config = function()
vim.keymap.set("n", "<leader>hC", "<cmd>Legendary<cr>", { desc = "Command Palette" })

require("legendary").setup {
  extensions = {
    which_key = { auto_register = true },
    nvim_tree = true
  },
}
