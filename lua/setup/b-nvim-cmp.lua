local cmp = require "cmp"
local luasnip = require "luasnip"
local icons = require "config.icons"
local compare = require "cmp.config.compare"
local source_names = {
  nvim_lsp = "[LSP]",
  path = "[Path]",
  luasnip = "[Snippet]",
  buffer = "[Buffer]",
  rg = "[Ripgrep]",
  calc = "[Calc]",
  spell = "[Spell]",
}
local duplicates = {
  buffer = 1,
  path = 1,
  nvim_lsp = 0,
  luasnip = 1,
}

-- I think this screwed up working on tsv files;  not totally sure but caused some issue.
local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match "%s" == nil
end

cmp.setup({
  completion = {
    completeopt = "menu,menuone,noselect",
  },
  sorting = {
    priority_weight = 2,
    comparators = {
      compare.score,
      compare.recently_used,
      compare.offset,
      compare.exact,
      compare.kind,
      compare.sort_text,
      compare.length,
      compare.order,
    },
  },
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert {
    ["<C-b>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-Space>"] = cmp.mapping.complete(),
    ["<C-e>"] = cmp.mapping.abort(),
    ["<CR>"] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
        -- You could replace the expand_or_jumpable() calls with expand_or_locally_jumpable()
        -- they way you will only jump inside the snippet region
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, {
      "i",
      "s",
      "c",
    }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {
      "i",
      "s",
      "c",
    }),
  },
  sources = cmp.config.sources {
    { name = "nvim_lsp", group_index = 1, max_item_count = 15 },
    { name = "luasnip",  group_index = 1, max_item_count = 8 },
    {
      name = "spell",
      option = { keep_all_entries = true },
      group_index = 3,
      max_item_count = 20,
    },
    { name = "buffer",  group_index = 2 },
    { name = "path",    group_index = 2 },
    { name = "git",     group_index = 2 },
    { name = "orgmode", group_index = 2 },
    { name = "rg",      group_index = 2 },
  },
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, item)
      local max_width = 0
      local duplicates_default = 0
      if max_width ~= 0 and #item.abbr > max_width then
        item.abbr = string.sub(item.abbr, 1, max_width - 1) .. icons.ui.Ellipsis
      end
      item.kind = icons.kind[item.kind]
      item.menu = source_names[entry.source.name]
      item.dup = duplicates[entry.source.name] or duplicates_default

      return item
    end,
  },
  window = {
    documentation = {
      border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
      winhighlight = "NormalFloat:NormalFloat,FloatBorder:TelescopeBorder",
    },
  },
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.filetype("yaml", {
  sources = cmp.config.sources({
    { name = "luasnip" },
  }, {
    { name = "calc" },
  }),
})

cmp.setup.filetype("ledger", {
  sources = cmp.config.sources {
    { name = "hledger" },
    { name = "calc" },
  },
})

cmp.setup.filetype("asciidoc", {
  sources = cmp.config.sources {
    { name = "nvim_lsp" },
    { name = "luasnip" },
    { name = "spell" },
    {
      name = "buffer",
      option = {
        keyword_pattern = [[\k\+]],
      },
    },
  },
})

cmp.setup.filetype("orgmode", {
  sources = cmp.config.sources({
    { name = "nvim_lsp" },
    { name = "luasnip" },
    { name = "orgmode" },
    { name = "spell" },
    {
      name = "buffer",
      option = {
        keyword_pattern = [[\k\+]],
      },
    },
  }, {
    { name = "calc" },
  }),
})

cmp.setup.filetype("norg", {
  sources = cmp.config.sources({
    { name = "luasnip" },
    { name = "neorg" },
    { name = "spell" },
    {
      name = "buffer",
      option = {
        keyword_pattern = [[\k\+]],
      },
    },
  }, {
    { name = "calc" },
  }),
})

cmp.setup.cmdline({ "/", "?" }, {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = "buffer" },
  },
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(":", {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = "cmdline" },
  }),
})

-- Auto pairs
local has_autopairs, cmp_autopairs = pcall(require, "nvim-autopairs.completion.cmp")
if has_autopairs then
  cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done { map_char = { tex = "" } })
end

-- Git
require("cmp_git").setup { filetypes = { "NeogitCommitMessage" } }
