local components = require "config.statusline.components"
require('lualine').setup {
        options = {
          icons_enabled = true,
          theme = "oxocarbon",
          component_separators = {},
          section_separators = {},
          disabled_filetypes = {
            statusline = {
              "qf",
              "noice",
              "TelescopePrompt",
              "mason",
            },
            winbar = {
              "help",
              "alpha",
              "lazy",
            },
          },
          always_divide_middle = true,
        },
        sections = {
          lualine_a = {
            components.mode2,
          },
          lualine_b = {
            components.git_repo,
            components.branch,
          },
          lualine_c = {
            components.filename,
            components.diff2,
            components.diagnostics,
            components.recording,
            components.python_env,
            -- components.language,
          },
          lualine_x = {
            components.treesitter,
            components.lsp_client,
            -- components.schema,
            components.filetype,
          },
          lualine_y = {
            components.hexchar,
            components.keymap,
            components.location,
            components.csv,
          },
          lualine_z = {
            components.scrollbar,
          },
        },
        inactive_sections = {
          lualine_a = {},
          lualine_b = {},
          lualine_c = { "filename" },
          lualine_x = { "location" },
          lualine_y = {},
          lualine_z = {},
        },
        extensions = {
          "nvim-tree",
          "toggleterm",
          "quickfix"
        },
}
vim.opt.laststatus = 3
