      require("nvim-autopairs").setup {
        -- will intefere with french quotes
        -- disable_filetype = { "asciidoctor", "asciidoc", "markdown", "norg" },
        enable_check_bracket_line = false, -- Don't add pairs if it already has a close pair in the same line
        ignored_next_char = "[%w%.]", -- will ignore alphanumeric and `.` symbol
        check_ts = true, -- use treesitter to check for a pair.
        ts_config = {
          lua = { "string" }, -- it will not add pair on that treesitter node
          javascript = { "template_string" },
          java = false, -- don't check treesitter on java
        },
      }
      require("cmp").setup {}
      -- If you want insert `(` after select function or method item
      local cmp_autopairs = require "nvim-autopairs.completion.cmp"
      local cmp = require "cmp"
      cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done { map_char = { tex = "" } })

      --add custom rules here
      -- local rule = require("nvim-autopairs.rule")
      -- local autopairs = require("nvim-autopairs")
      --basically works but cursor will remain before end quote
      -- autopairs.add_rules({
      --   -- rule("« ", " »", { "asciidoc", "markdown" }),
      --   rule('"', " »"):set_end_pair_length(2):replace_endpair(function()
      --     return "<BS>«  »"
      --   end),
      -- })
