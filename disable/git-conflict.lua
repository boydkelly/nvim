    -- "akinsho/git-conflict.nvim",
    -- --stylua: ignore
    -- keys = {
    --   -- {"<leader>gT", function() require("plugins.hydra.git-action").open_git_conflict_hydra() end, desc = "Conflict"},
    --   -- {"<leader>gS", function() require("plugins.hydra.git-action").open_git_signs_hydra() end, desc = "Signs"},
    -- },
    -- cmd = {
    --   "GitConflictChooseBoth",
    --   "GitConflictNextConflict",
    --   "GitConflictChooseOurs",
    --   "GitConflictPrevConflict",
    --   "GitConflictChooseTheirs",
    --   "GitConflictListQf",
    --   "GitConflictChooseNone",
    --   "GitConflictRefresh",
    -- },
local opts = {
      default_mappings = true,
      default_commands = true,
      disable_diagnostics = true,
    }
require("git-conflict").setup(opts)
