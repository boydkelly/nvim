require "config.lazy"
require "config.options"
-- some functions like return cursor to last place won't work if these are in the autocmd below
-- require "utils"
require "config.autocmds"
require "config.keymaps"
require "config.commands"
vim.api.nvim_create_autocmd("User", {
  pattern = "VeryLazy",
  callback = function()
    local stats = require("lazy").stats()
    local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
    -- local now = os.date "%d-%m-%Y %H:%M:%S"
    local version = "  v" .. vim.version().major .. "." .. vim.version().minor .. "." .. vim.version().patch
    local plugins = "⚡ Neovim " .. version .. " loaded " .. stats.count .. " plugins in " .. ms .. " ms "
    if ms > 100 then
      vim.notify(plugins)
    end
  end,
})
